---
title: Support Information
weight: 3
draft: false
---

See the following support information, learning channels, and Verrazzano release history:

* Verrazzano Community Support - [Join Verrazzano on Slack!](https://bit.ly/verrazzano-slack)
* [Verrazzano GitHub Project](https://github.com/verrazzano/verrazzano/issues)
* [Oracle Verrazzano Enterprise Container Platform Support](https://support.oracle.com/epmos/faces/DocumentDisplay?id=2794708) (available for purchase; contact [Oracle Sales](https://www.oracle.com/corporate/contact/))
* [Check us out on Medium](https://medium.com/verrazzano)
* [Learn about Verrazzano on YouTube](https://www.youtube.com/@verrazzano_io)


<b>Release History & Error Correction Dates<b>

The timeline for Verrazzano releases and the date of their end of error correction.
| Verrazzano                                                          | Release Date | Latest Patch Release                                                  | Latest Patch Release Date | End of Error Correction |
|---------------------------------------------------------------------|--------------|-----------------------------------------------------------------------|---------------------------|-------------------------|
| [1.6](https://github.com/verrazzano/verrazzano/releases/tag/v1.6.0) | 2023-06-28   | [1.6.7](https://github.com/verrazzano/verrazzano/releases/tag/v1.6.7) | 2023-09-25                | 2024-06-30*             |
| [1.5](https://github.com/verrazzano/verrazzano/releases/tag/v1.5.0) | 2023-02-15   | [1.5.7](https://github.com/verrazzano/verrazzano/releases/tag/v1.5.7) | 2023-09-25                | 2024-02-28              |
| [1.4](https://github.com/verrazzano/verrazzano/releases/tag/v1.4.0) | 2022-09-30   | [1.4.8](https://github.com/verrazzano/verrazzano/releases/tag/v1.4.8) | 2023-09-25                | 2023-10-31              |
| [1.3](https://github.com/verrazzano/verrazzano/releases/tag/v1.3.0) | 2022-05-24   | [1.3.8](https://github.com/verrazzano/verrazzano/releases/tag/v1.3.8) | 2022-11-17                | 2023-05-31              |
| [1.2](https://github.com/verrazzano/verrazzano/releases/tag/v1.2.0) | 2022-03-14   | [1.2.2](https://github.com/verrazzano/verrazzano/releases/tag/v1.2.2) | 2022-05-10                | 2022-11-30              |
| [1.1](https://github.com/verrazzano/verrazzano/releases/tag/v1.1.0) | 2021-12-16   | [1.1.2](https://github.com/verrazzano/verrazzano/releases/tag/v1.1.2) | 2022-03-09                | 2022-09-30              |
| [1.0](https://github.com/verrazzano/verrazzano/releases/tag/v1.0.0) | 2021-08-02   | [1.0.4](https://github.com/verrazzano/verrazzano/releases/tag/v1.0.4) | 2021-12-20                | 2022-06-30              |

*Projected date. Actual date will be determined when the next minor or major release is available.
<br>
